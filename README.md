# music_recommender
A music recommendation system using the Last.fm API and the LightFM library. 

## Dependencies

1. python3
1. lightfm 
1. scipy
1. numpy


Run command in terminal:

```
pip install -r requirements.txt
```

## Usage

Download the dataset file [here](https://www.upf.edu/web/mtg/lastfm360k). Move the dataset file to the music_recommender directory. The presumed file path is:

```
/music_recommender/lastfm-dataset-360K.tar.gz
```

Run script in terminal:

```
python music_recommender.py
```

Enter your top 3 favorite artists before Fall 2008. Prints artists that are found in the dataset.

Result is top 10 artist recommendations predicted by the model.
