import numpy as np
import sys
from get_lastfm import get_lastfm, my_artists

# ignore UserWarning
import warnings
warnings.filterwarnings('ignore',category=UserWarning)

from lightfm import LightFM

# get data matrix, artists, and users from get_lastfm
data = get_lastfm()

# apply model to data set
model = LightFM(loss='bpr')
model.fit(data['matrix'], epochs=45, num_threads=2)

# print precision of model 
# from lightfm.evaluation import precision_at_k
# print("model precision: %.3f" % precision_at_k(model, data['matrix'], k=5).mean())

# get recommendations from model 
def get_recommendations(model, coo, user):

    # makes prediction based on end user values
    n = coo.shape[1]
    try: 
        scores = model.predict(user, np.arange(n))
    except ValueError:
        print('entered artists not found')
        sys.exit()
    
    # get top 15 recommendations
    top_recommendations = []
    values = list(data['artists'].values())
    top_scores = np.argsort(-scores)[:15]
    for score in top_scores:
        recommendation = values[score]['name']
        if recommendation == my_artists[0]: continue
        elif recommendation == my_artists[1]: continue
        elif recommendation == my_artists[2]: continue
        else: top_recommendations.append(recommendation)
    
    # disply top 10 recommendations 
    print('Your top 10 music recommedations are:')
    for i in range(10):
        print(' - %s' % top_recommendations[i])

get_recommendations(model, data['matrix'], data['users'])
